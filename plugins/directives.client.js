import Vue from 'vue'

// v-focus
Vue.directive('focus', {
  // When the bound element is inserted into the DOM...
  inserted(el, binding) {
    if (!('value' in binding) || binding.value) el.children[0].focus()
  },
})

// v-prevent-last-char-break
Vue.directive('prevent-last-char-break', {
  inserted(el, binding) {
    const txt = el.innerHTML.trim()
    if (/.*\s[?!:;]$/.test(txt))
      el.innerHTML = txt.substr(0, txt.length - 2) + '&nbsp;' + txt.substr(-1)
  },
})
