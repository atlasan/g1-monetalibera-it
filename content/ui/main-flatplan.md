---
title: Chemin de fer
description: Parcours de lecture. Affiché sous le héro de la page d'accueil, et dans les pages concernées.
layout: cdc
---

# [1. Découvrir](/decouvrir)

- D'où vient l'argent ?
- La création monétaire en monnaie libre

---

# [2. Comprendre](/comprendre)

- La théorie relative de la monnaie
- La toile de confiance et la blockchain de la Ğ1

---

# [3. Débuter](/debuter)

  - Ouvrir un compte
  - Les outils de la Ğ1

---

# [4. Contribuer](/contribuer)

- Rencontrer des utilisateurs
- Participer
