---
title: Introduction page d'accueil
description: Texte d'introduction en page d'accueil sous la section de recherche
---


<img src="/uploads/logo-monnaie-libre.svg" align ="right" style="width:300px;height:300px;margin:30px" >

## Encore une nouvelle monnaie

Une monnaie est un outil d'échange, de mesure et de réserve de valeur.\
Tout peut être une monnaie, c'est l'usage qui fait d'un objet une monnaie. Les céréales (dont le blé), le sel (d'où vient le mot salaire) ont longtemps servi de monnaie. Puis sont apparues les monnaies fiduciaires (basées uniquement sur la confiance). **En utilisant la monnaie dette vous faites confiance aux banquiers.**

Il faut se poser la question de la provenance de la monnaie. D'où vient-elle? Comment est-elle créée ? Par qui ?

## Une monnaie différente

La monnaie libre est créée uniquement dans le portefeuille des êtres humains vivants.\
La création de monnaie libre est répartie équitablement entre tous les membres de la communauté, dans l'espace et dans le temps.\
C'est-à-dire que quel que soit le lieu mais aussi quelle que soit l'époque, chaque membre crée la même portion de monnaie, durant toute sa vie.

La même portion ne veux pas dire même quantité : quand la quantité de monnaie en circulation augmente, la portion créée par chacun augmente aussi.

## Une monnaie relative

Il faut rester conscient que ce n'est pas la quantité de monnaie que l'on possède qui compte, mais la part relative de monnaie par rapport à la masse monétaire globale.\
En monnaie libre, on ne compte plus en quantitatif mais en relatif. On utilise une unité relative de monnaie : le <lexique>DU</lexique> quotidien.