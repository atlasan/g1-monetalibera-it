---
title: Héro page d'accueil
---

<section class="xl:mr-20">
  <h1 class="mb-4 text-4xl md:text-5xl xl:text-6xl bg-clip-text text-transparent bg-gradient-to-r from-purple-800 to-blue-600 font-extrabold leading-tight slide-in-bottom-h1">
  La monnaie libre
  </h1>

  <p class="leading-normal text-xl text-gray-700 dark:text-gray-300 md:text-2xl xl:text-4xl mb-12 font-semibold slide-in-bottom-subtitle">
  Repenser la création monétaire...<br />et l'expérimenter !
  </p>

  <p class="leading-normal text-base text-gray-700 dark:text-gray-300 xl:text-xl mb-6 slide-in-bottom-subtitle">
  Un modèle économique plus juste et durable est possible.<br />Une monnaie libre place l'humain au cœur de l'économie et prend en compte les générations futures.
  </p>

  <p class="leading-normal text-base text-gray-700 dark:text-gray-300 xl:text-xl mb-6 slide-in-bottom-subtitle">
  La Ğ1 (la "June") est la première monnaie libre. Conçue sur une blockchain écologique, c'est une expérience citoyenne, solidaire... et peut-être subversive !
  </p>
</section>