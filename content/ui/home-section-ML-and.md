---
title: Section monnaie libre et...
dev-notes: It's look ugly for now, but it will be better and easier to build block in next version with vuejs component in markdown...
---
<nuxt-link to="/la-monnaie-libre-et-lenvironnement" class="group block mt-6 lg:mt-12 p-8 hover:shadow-xl rounded-lg transition transform hover:-translate-y-1">
  <section class="flex flex-col md:flex-row items-center gap-8">
      <img src="/img/healthy-eating.svg" class="w-4/5 md:w-2/5" />
      <div>
        <h2 class="text-purple-800 dark:text-purple-600 text-2xl lg:text-3xl mb-4">L'environnement</h2>
        <p class="text-base lg:text-lg">La Ğ1 n'utilise pas une puissance de calcul pour sécuriser la blockchain. La création monétaire par DU permet de ne plus passer par une dette perpétuelle pour créer la monnaie. Produire à tout prix n'est plus une fin en soi. De plus, le DU est une première marche vers un mode de vie sobre.</p>
        <div class="opacity-100 md:opacity-0 transition group-hover:opacity-100 underline pt-4 group-hover:text-hover">En savoir plus...</div>
      </div>
  </section>
</nuxt-link>

<nuxt-link to="/la-monnaie-libre-et-la-democratie" class="group block mt-6 lg:mt-12 p-8 hover:shadow-xl rounded-lg transition transform hover:-translate-y-1">
  <section class="flex flex-col md:flex-row items-center gap-8 flex-col-reverse">
    <div>
      <h2 class="text-purple-800 dark:text-purple-600 text-2xl lg:text-3xl mb-4">La démocratie</h2>
      <p class="text-base lg:text-lg">Laissez-moi la planche à billet, je me fiche de savoir qui fait les lois. Le privilège de création monétaire est un pouvoir. Répartir la création sur l'ensemble de la population est bien plus démocratique.</p>
      <div class="opacity-100 md:opacity-0 transition group-hover:opacity-100 underline pt-4 group-hover:text-hover">En savoir plus...</div>
    </div>
    <img src="/img/work-life-balance.svg" class="w-3/5 md:w-1/4" />
  </section>
</nuxt-link>

<nuxt-link to="/la-monnaie-libre-et-les-crypto-monnaies" class="group block mt-6 lg:mt-12 p-8 hover:shadow-xl rounded-lg transition transform hover:-translate-y-1">
  <section class="flex flex-col md:flex-row items-center gap-8">
    <img src="/img/High-quality-products.svg" class="w-3/5 md:w-1/3" />
    <div>
      <h2 class="text-purple-800 dark:text-purple-600 text-2xl lg:text-3xl mb-4">Les crypto-monnaies</h2>
      <p class="text-base lg:text-lg">La Ğ1 est une crypto-monnaie, mais son modèle de création monétaire en fait une monnaie bien plus équitable que les autres cryptos. Les premiers arrivés ne sont pas privilégiés par rapport aux nouveaux entrants.</p>
      <div class="opacity-100 md:opacity-0 transition group-hover:opacity-100 underline pt-4 group-hover:text-hover">En savoir plus...</div>
    </div>
  </section>
</nuxt-link>

<nuxt-link to="/la-monnaie-libre-et-les-monnaies-locales" class="group block mt-6 lg:mt-12 p-8 hover:shadow-xl rounded-lg transition transform hover:-translate-y-1">
  <section class="flex flex-col md:flex-row items-center gap-8 flex-col-reverse">
    <div>
      <h2 class="text-purple-800 dark:text-purple-600 text-2xl lg:text-3xl mb-4">Les monnaies locales</h2>
      <p class="text-base lg:text-lg">Généralement, les monnaies locales sont indexés à l'Euro. Ce n'est pas un changement de référentiel. Je suis pauvre en Euro, je suis pauvre en monnaie locale.</p>
      <div class="opacity-100 md:opacity-0 transition group-hover:opacity-100 underline pt-4 group-hover:text-hover">En savoir plus...</div>
    </div>
    <img src="/img/MLC.svg" class="w-3/5 md:w-1/3" />
  </section>
</nuxt-link>
