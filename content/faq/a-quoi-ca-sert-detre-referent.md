---
title: Qual è il punto di essere un referente?
description: I referenti rafforzano la rete di fiducia.
---
## Questo non porta alcun vantaggio
I co-creatori <lexique title="referenti">referenti</lexique> non hanno diritti in piú, nessun privilegio, non sono nientaltro che persone di riferimento che conoscono la moneta libera.
Possiamo solo supporre che siano più attivi nella rete di fiducia e che abbiano un pó di esperienza, perché hanno **ricevuto ed emesso** un certo numero di certificazioni.

## Per la regola della distanza
È stato dimostrato che, sul pianeta, [tutti gli umani sono a meno di 5 o 6 passi l'uno dall'altro](https://fr.wikipedia.org/wiki/Six_degr%C3%A9s_de_s%C3%A9paration) (conosco qualcuno che conosce qualcuno che conosce qualcuno ...).
Per mantenere una certa forma di sicurezza nella rete di fiducia e in particolare per facilitare i calcoli per i server Duniter, é stato giudicato, per la rete di fiducia della G1, que se il 100% dei membri potrebbero unirsi al 80% dei membri **referenti** in meno di 5 passi, questo sará sufficente.
É questa quella che viene chiamata la <lexique>regola della distanza</lexique>: Una persona può essere un membro solo se può raggiungere almeno l'80% dei membri referenti in 5 passi o meno.
Un membro referente é un membro che ha emesso **e** ricevuto un certo numero di certificazioni in funzione della dimensione della rete di fiducia.

Piú informazioni su [questa presentazione di Eloïs](https://www.youtube.com/watch?v=8GaTKfa-ADU) durante RML11 di Douarnenez nel 2018.

Guardare il lessico per conoscere il numero di certificazioni necessarie per essere <lexique>referente</lexique>.