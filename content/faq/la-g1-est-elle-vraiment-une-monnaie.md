---
title: La Ğ1 è davvero una moneta?
description: Sì, ma non per la legge.
---


Dipende dalla definizione! 
Se definiamo una moneta come un facilitatore di scambio, un'unità di misurazione del valore, allora **si**.

Normalmente le leggi nazionali non riconoscono la Ğ1 come una moneta (lo stesso vale per Bitcoin e le monete locali). Tuttavia, la legge nomrlmanete è normativa e non descrittiva e non è né sufficiente né rilevante per descrivere il mondo.

La Ğ1 non è materializzata in biglietti. Ciò non significa che non sia una moneta: il denaro non è necessariamente materiale, la maggior parte degli euro sono solo numeri digitali. Inoltre, le banconote non sono propriamente moneta: non sono nient'altro che il riconoscimento del debito che la banca centrale emette, gli euro "veri" sono i valori informatici nei computer!
