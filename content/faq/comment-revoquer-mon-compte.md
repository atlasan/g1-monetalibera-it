---
title: Comment révoquer mon compte
description: Uniquement pour les compte cocréateurs de monnaie
---
## Uniquement pour les comptes créateurs de monnaie
Révoquer un compte, cela veut dire qu'il ne pourra plus créer de monnaie ni émettre de certifications.  
Révoquer un compte qui n'est pas encore créateur de monnaie (en cours de certification) empêchera la certification sur ce compte via césium et entrainera sa révocation immédiate s'il parvient à être validé.    
S'il le compte n'est pas membre, ni en phase de devenir membre, il n'y a pas de possibilité de le révoquer.

## Par les options dans "mon compte"
Quand vous êtes correctement identifié, il n’y a pas besoin du fichier de révocation.  
Allez sur "Mon compte" cliquez sur "options" puis "compte et sécurité..." et enfin "Révoquer immédiatement ce compte"  

Cette procédure est utile quand vous pensez que vos identifiant et mot de passe ont pu être volé ou piraté ou que vous pensez qu'ils ne sont pas assez sécurisés, ou bien quand vous voulez changer de "pseudonyme" c'est-à-dire d'identifiant Duniter. 



## Par le lien "mot de passe oublié"
Quand vous avez perdu vos mots de passes, au moment de vous identifier il y a un lien « mot de passe oublié ? » en cliquant sur ce lien vous avez le choix entre « retrouver mon mot de passe » (si vous avez créé un fichier de sauvegarde de vos identifiants) ou « révoquer mon compte membre » (à partir d’un fichier de révocation que vous avez préalablement [téléchargé](/faq/comment-telecharger-le-fichier-de-revocation)