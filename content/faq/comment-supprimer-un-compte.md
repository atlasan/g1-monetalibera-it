---
title: Comment supprimer un compte
description: On peut seulement supprimer le profil
---
## On ne peut pas supprimer un compte

Dès qu'un compte reçoit une transaction, il apparait dans la blockchain et y restera pour toujours.  
*On ne peut pas fermer ni clôturer ce compte.*

## Compte cocréateur (membre)

Un compte membre peut être révoqué. Ce n'est pas une suppression de compte.   
La révocation ne fait que transformer le compte cocréateur en compte simple. Il ne créera plus de DUğ1 et ne pourra plus émettre de certification.

Voir [Comment révoquer mon compte](/faq/comment-revoquer-mon-compte)


## Compte simple (portefeuille)

On peut seulement supprimer le profil qui est associé à ce compte, dans césium dans l'onglet "mon compte" cliquez sur "éditer mon profil"  puis sur "options" puis "supprimer mon profil". 
 
