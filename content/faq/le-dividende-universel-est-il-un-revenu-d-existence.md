---
title: Le dividende universel est-il un revenu d’existence ?
description: Un revenu de base, mais pas de suffisance !
---

# Un revenu de base ou revenu universel

Dans une monnaie libre, le <lexique>Dividende Universel</lexique> (DU) est le seul moyen par lequel peuvent être créé des unités monétaires.

En ce sens, on peut dire que c’est un [revenu de base][rdb], puisqu’il est le socle (la “base”) de la création monétaire. Le DU est créé par tous (par “la base") sur une base individuelle et avec seule condition d'être en vie. Il n'est pas créé uniquement par ceux qui sont en haut des pyramides monétaires.

Pour la Ğ1, le DU est créé chaque jour par tous les membres de la toile de confiance. En ce sens, on peut donc dire qu’il est un “revenu universel” (du moins pour les membres de la toile de confiance).

# Mais pas de suffisance !

En revanche, qu’il soit ou non possible de vivre avec le DU n’est pas défini dans le code monétaire d'une monnaie libre.

Le montant d'un tel DU dépendrait de chacun, car tout le monde n’a pas les mêmes besoins.

À notre connaissance, actuellement, personne n’a encore réussi à subsister avec son seul DU.

Peut-être que dans quelques années, lorsque la Ğ1 aura beaucoup plus de membres qui produisent des biens et des services, et que la concurrence entre producteurs aura fait baisser les prix de marché, il sera possible de vivre uniquement avec le DU. Actuellement, on n’en est pas là.

[rdb]: https://fr.wikipedia.org/wiki/Revenu_de_base#:~:text=Le%20revenu%20de%20base%2C%20encore,obligation%20ou%20absence%20de%20travail.
