---
title: " C’est pas un peu utopiste ?"
description: "Au contraire : la Ğ1 est complètement ancré dans la réalité."
---
### La Ğ1 est une expérimentation concrète. 

La Ğ1 existe depuis quelques temps et elle a déjà impacté la vie de pas mal de gens, parfois de façon négative, mais la plupart du temps de façon positive.

Certains se seront trop impliqués trop vite et ont fini déçus.

D’autres, étaient étaient persuadés au départ que la Ğ1 était une arnaque mais, par curiosité, ils l’ont expérimentée pour eux-même, et ont fini par faire énormément d’échanges, de rencontres, et en sont ravis.

### Laissons lui le temps de croître.

Alors, bien sûr, le projet Ğ1 va prendre du temps à gagner en popularité.

La Ğ1 est un projet dont les développeurs voient les choses à très long terme (plusieurs générations).

En attendant, les <lexique>jünistes</lexique> changent déjà le monde, un échange à la fois.
