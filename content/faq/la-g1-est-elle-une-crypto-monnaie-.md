---
title: " La Ğ1 est-elle une « crypto-monnaie » ?"
description: Oui, mais pas que ...
---
Vous pouvez, si vous voulez, qualifier la Ğ1 de “crypto-monnaie”, dans le sens où la blockchain est effectivement basée sur de la cryptographie.

Mais ce serait oublier que la cryptographie n’est l’apanage ni de la blockchain ni des monnaies que la presse généraliste appelle “crypto-monnaies”.

La cryptographie intervient tout aussi bien pour sécuriser n’importe quelle transaction numérique, qu’elle soit en euros ou dans n’importe quelle autre devise.

Mais une monnaie libre pourrait aussi bien être gérée avec du papier ou des tablettes d'argile, ce serait moins pratique, et nécessiterait sans doute un organisme centralisateur. 
