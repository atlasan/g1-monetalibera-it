---
title: La messagerie césium est-elle cryptée ?
description: Le message est chiffré et signé de bout en bout à l'aide de vos clés
---
## La messagerie Cesium+ est chiffré

Tout d'abord, [chiffré](https://www.cnrtl.fr/definition/chiffr%C3%A9) est plus correct que [crypté](https://www.cnrtl.fr/definition/crypt%C3%A9).  
[Comprendre la différence entre "chiffré" et "crypté"](https://tutox.fr/2017/04/28/chiffrer-nest-crypter/).

La messagerie Césium est indépendante de Duniter, elle est gérée par les [nœuds Césium+](https://forum.g1-monetalibera.it/t/noeud-cesium-vs-noeud-duniter/14762).\
Les messages directs entre utilisateurs Ğ1 sur Cesium sont chiffrés de bout en bout. 

Sur votre machine, le message est *chiffré* vers la clé publique de votre destinataire et *signé* à l'aide de votre clé privée ed25519 (créée à partir de votre identifiant secret et votre mot de passe), de sorte que le serveur Cesium+ ne peut pas le lire (car chiffré) ni le falsifier (car signé). Il est ensuite déchiffré en utilisant la clé privée de votre destinataire, sur sa machine. C'est pour cela qu'on parle de "bout en bout".\
Précisément, il utilise la cryptobox TweetNaCl.

Cependant, certaines métadonnées (comme les comptes de départ et d'arrivée) sont *en clair* (non chiffrées), car le serveur doit en informer le destinataire.

## Les commentaires des transactions Duniter sont en clair

Dans la première version de Duniter les commentaires des transactions sont stocké en clair dans la blockchain.\
Dans la version 2 de Duniter, ils seront, eux aussi, chiffrés.