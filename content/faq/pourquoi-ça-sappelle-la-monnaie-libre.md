---
title: " Perché si chiama la moneta libera” ?"
description: La moneta che rende liberi
---
Nella Teoria Relativa della Moneta, Stéphane Laborde definisce la moneta libera come concorde alle seguenti quattro libertà economiche:

0. Libertà di scelta del sistema monetario 
1. Libertà di utilizzare le risorse 
2. Libertà di stima e produzione di qualsiasi valore economico 
3. Libertà di scambiare, comptabiliser afficher ses prix “dans la monnaie””

**Parlando di libertá si intende sempre:**

* nessun danno dagli altri nei confronti di se stessi 
* nessun danno da se stessi nei confronti degli altri

**Dans un système monétaire libre :**

* la symétrie spatiale permet à un individu d’éviter de nuire à ses contemporains,
* la symétrie temporelle permet à une génération d’éviter de nuire aux suivantes.

Les 4 libertés fondamentales de la monnaie libre sont directement inspirées de celles du [logiciel libre](https://vive-gnulinux.fr.cr/logiciel-libre/4-libertes/), telles que les a définies Richard Stallman.   
![](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Galuel_RMS_-_free_as_free_speech%2C_not_as_free_beer.png/800px-Galuel_RMS_-_free_as_free_speech%2C_not_as_free_beer.png "Stéphane Laborde en compagnie de Richard Stallman pour illustrer la citation \"free as in free speech, not as in free beer\"")
Stephane Laborde en compagnie de Richard Stallman pour illustrer la citation :  
“Free as in free speech, not as in free beer”  
(source : [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Galuel_RMS_-_free_as_free_speech,_not_as_free_beer.png?uselang=fr), licence Creative Commons BY SA)
Navigation
