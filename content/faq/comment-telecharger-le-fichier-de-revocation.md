---
title: Comment télécharger le fichier de révocation
description: Uniquement sur ordinateur avec césium
---
## Compte membre uniquement.
Le document de révocation ne concerne que les comptes cocréateurs (membres), ou en passe de devenir cocréateur.
Pas la peine de chercher à le télécharger depuis un simple portefeuille, ça n'existe pas.

## Uniquement sur ordinateur.
Le téléchargement du fichier de révocation ne fonctionne pas sur smartphone et tablettes (ça vous fait croire que c'est bon, mais en fait non, c'est un bug)

## Procédure dans cesium.

### Lors de la transformation du compte en compte membre
Quand vous transformer votre compte en compte membre, le logiciel vous invite à télécharger le fichier de révocation.
Attention cela ne fonctionne vraiment que sur un ordinateur (pas sur tablette ni smartphone). Si vous avez fait la demande de transformation en compte membre depuis un smartphone ou tablette, vous devrez, refaire le téléchargement de ce fichier sur un ordinateur.

### Une fois devenu membre
Vous pouvez à tout moment refaire une sauvegarde de ce fichier de révocation.
**Seulement sur ordinateur**
* Allez sur la page "mon compte"
* Cliquez sur "Options"
* Cliquez sur "compte et sécurité"
* Cliquez sur "sauvegarder mon fichier de révocation"
* Authentifiez-vous (saisie d'identifiant et mot de passe)

### Tuto vidéo
<iframe title="Sauvegarder le certificat de révocation (Uniquement sur ordinateur) " src="https://video.tedomum.net/videos/embed/a5dbfeca-f994-4278-acd3-49a1bf4c9fed" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups" width="100%" height="405" frameborder="0"></iframe>


## Sauvegardez ce fichier pour ne jamais le perdre
Ne laissez pas ce fichier uniquement sur le disque du dur de votre ordinateur, sauvegardez le en différents endroits, clé USB, carte SD ou autre. Vous pouvez aussi mettre ce fichier sur le cloud.
