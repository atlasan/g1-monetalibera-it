---
title: Est-ce que les premiers membres sont privilégiés par rapport aux nouveaux ?
description: Oui mais non.
---
## Il faut commencer par vendre

Si vous arrivez dans un système d’échange dans lequel certains ont de la monnaie mais pas vous, vous devrez commencer par vendre avant de pouvoir acheter.

Les utilisateurs qui produisent des Ğ1 depuis quelque temps ont donc sur vous l’avantage de pouvoir acheter avant de vendre. **Surtout s'ils ont déjà vendu beaucoup de biens ou services.**

Ils peuvent même acheter sans vendre. Mais s’ils font ça indéfiniment, le solde de leur compte va évidemment tendre vers 0, donc c’est rapidement limité comme pratique, d’autant plus limité si les prix des biens et services représente un nombre conséquent de jours de création monétaire.

Typiquement, si un pot de confiture coûte 6 DU, et étant donné qu’il y a en à peu près 30 jours dans un mois, cela signifie qu’un membre de la toile de confiance Ğ1 peut acheter, chaque mois, 5 confitures sans rien vendre.

Est-ce vraiment une situation si enviable ?

## Vous pouvez acheter à crédit

Par ailleurs, il peut arriver, si vous êtes à un moment dépourvu de Ğ1, mais souhaitez acheter un bien, par exemple lors d’un <lexique title="ğévènement">ğmarché<lexique>, que le vendeur accepte que vous le payiez plus tard.
*Vous pouvez aussi emprunter des Ğ1 auprès d'autres utilisateurs*

### Pourquoi refuserait-il ?

Il sait très bien que vous aller produire de la Ğ1 dans l’avenir ; il n’a donc pas de raison de douter de vos capacités de remboursement.

## Finalement tout le monde créé la même part

Avec la convergence des comptes, tous les utilisateurs auront créé la même portion de monnaie (le même nombre de DUğ1) quelque soit leur date d'entrée dans la cocréation de la monnaie.

![](/uploads/convergence-des-soldes.png)