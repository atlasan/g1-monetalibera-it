---
title: Renouvellement des Certifications
description: Une certification expire au bout de deux ans
---
## Certifications reçues
Une <lexique>certification</lexique> est valable deux ans, si vous n'avez pas 5 certifications en cours de validité, vous perdez votre statut de cocréateur de monnaie. 

N'attendez pas que vos 5 premières certifications arrivent à expiration pour en chercher de nouvelles.
Si cela arrive, il suffit de retrouver 5 personnes pour vous certifier en respectant la <lexique>règle de distance</lexique>. 

Faites vous certifier continuellement au fil de vos rencontres pour ne jamais être à court. Si vous vous faites certifier tous les 4 mois, en deux ans, vous en aurez 6, ce qui vous laisse le temps d'obtenir de nouvelles certifications quand les premières arrivent à échéance. 



## Certifications émises

Chaque cocréateur peut émettre 100 certifications. Quand une certification arrive à échéance, elle peut être émise à nouveau vers la même personne ou une autre.   
Un cocréateur peut donc émettre 100 certifications sur deux années glissantes. 

Une certification prise en compte (validée) rend les autres certifications (non validée) de l'émetteur indisponibles pendant 5 jours.
Évitez d'émettre plusieurs certifications en même temps, cela encombre les piscines.   
Une certification émise et prise en compte (validée) est valable deux ans à compter de la date d'émission. Évitez d'émettre plusieurs certifications en même temps, car elles arriveront à échéance en même temps, même si elles sont validées à 5 jours d'intervalle.  

## Certifier en permanence
Ne soyez pas avare de certification, certifiez toutes les personnes que vous connaissez. Vous aidez ainsi à resserrer la toile de confiance, vous augmentez la qualité des personnes que vous certifiez, et vous augmentez la qualité des dossiers des futurs entrants.   
Quand croisez un ami qui ne vous a pas encore certifié, n'hésitez à lui demander de réparer cet oubli.