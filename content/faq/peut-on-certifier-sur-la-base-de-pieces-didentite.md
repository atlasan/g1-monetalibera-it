---
title: Peut-on certifier sur la base de pièces d'identité ?
description: Sans intérêt.
---
On peut utiliser les papiers d'identités délivrés par l'état pour augmenter sa confiance dans le fait qu'une personne n'usurpe pas d'identité, cependant :
* l'exiger serait nier le droit à l'anonymat ou au pseudonymat ;
* l'état refuse de donner des papiers à certaines personnes, qui seraient donc injustement exclues de la Ğ1 ;
* trop se reposer sur ces papiers leur donnerait un énorme pouvoir sur la Ğ1 ;
* la monnaie deviendrait encore plus vulnérables aux instabilités de l'état ;
* franchement, remplacer un système social par une bureaucratie antidémocratique et anti-monnaie libre, ça vous plairait ?

La certification peut donner une apparence “sectaire” à la toile de confiance. Pourtant, que fait-elle de plus que l'état ? Ce dernier recense et surveille la population en usant de la force, principalement afin de prélever l'impôt et d'imposer sa politique. À côté, en certifiant un proche, on se contente de dire “cette personne existe et a compris et accepté la licence”.   
On est loin de la bureaucratie étatique !

Le même raisonnement s'applique à n'importe quel fournisseur d'identité centralisé, comme les GAFAM.
