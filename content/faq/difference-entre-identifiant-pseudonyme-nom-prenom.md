---
title: Différence entre identifiant, pseudonyme, identité, nom prenom
description: Suivant l'usage
---
## Identifiant Secret

C'est ce qui sert à vous connecter à votre compte. Ne le révélez jamais à personne. Tout comme votre phrase de passe.     
**Personne d'autre que vous ne doit les connaitre.**  

<alert type="danger" icon="skull-crossbones">Si vous le modifier (cet identifiant ou le mot de passe) vous changer de compte.</alert> 


## Pseudonyme (id-duniter)

Le champ "pseudonyme" dans césium est mal nommé, c'est en fait l'identifiant Duniter. Cet identifiant est unique. Une fois validé, **il ne pourra jamais être modifié**. 
Il permet à vos partenaires de vous retrouver quand il vous cherchent dans l'annuaire.  
*C'est en quelque-sorte votre identifiant public.*   

**Seuls les comptes cocréateurs ont un id-duniter validé.**   

<alert type="warning">Même si l'id suggérée est PrenomNom, choisissez plutôt un pseudonyme, car cette information est visible par le monde entier. Il faut rester discret sur internet, protégez votre vie privée.</alert>


## Identité (Id-duniter + date) 

Quand vous demander la transformation de votre compte en compte cocréateur(compte membre) **ou** quand vous demander la "création d'un compte membre" Vous faites une double action : vous publiez une identité **et** vous demandez une adhésion.   
Cette identité sera validée, quand vous aurez obtenu les 5 certifications respectant les règles de la <lexique nom=TdC>toile de confiance</lexique>. 
Si une identité n'est pas validée dans un délai de 2 mois (60 jours et 21 heures), elle disparait des piscines. Il faut alors recommencer le processus. 

### Double identité

Tant qu'une identité n'est pas validée, il est possible de publier une autre identité sur le même compte (en se connectant à un nœud dont la piscine n'est pas totalement synchronisée).  
Ce qui fait qu'un compte peu avoir temporairement une double identité (non validées).   
Dès qu'une des identités est validée, l'autre disparais.  
Attention à avoir conservé le bon [fichier de révocation](/faq/document-de-revocation), éventuellement le télécharger à nouveau pour plus de sureté. 





## Nom, prénom (on peut mettre un pseudo)

Le champ "nom et prénom" fait partie des données de césium+ comme toutes les données du profil qui sont visibles, elles aussi, par le monde entier.  
C'est pourquoi il est déconseillé d'y mettre votre véritable identité.  
Ces données peuvent être modifiées comme vous voulez en éditant votre profil.   

<alert type="warning">Ces champs étant modifiables, sans aucun contrôle, il est aisé d'usurper une identité. **Vérifier toujours la clé publique**</alert>
