---
title: Où trouver la Licence Ğ1 ?
description: Vous pouvez trouver la licence dans sur ce site
---
## Sur ce site

Vous pouvez trouver la licence sur ce site à la page [licence ğ1](/licence-g1) .

## Sur le site duniter

La licence est également disponible sur le site duniter.fr :\
<https://duniter.fr/wiki/g1/licence-txt/>

## Dans césium 
Sur ordinateur, allez dans l'onglet monnaies, vous trouverez un bouton "voir la licence" en haut de la page. 