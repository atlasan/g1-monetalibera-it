---
title: Quel est le taux de change Ğ1/Euro ?
description: "Il n'existe pas de taux de change pour la Ğ1. "
---

# Combien vaut 1 Ğ1 en euros ?

Difficile à dire.

Pour commencer : la Ğ1 n’est pas l’unité la plus pratique pour comparer la valeur de la monnaie par rapport à d’autres.

En effet, du fait que le nombre de Ğ1 soit en constante augmentation, la valeur d’une Ğ1 aura probablement tendance à baisser par rapport à toutes les autres valeurs économiques.

Il vaut donc mieux s’intéresser à la valeur d’un DU.

# Combien vaut 1 DU en euros ?

Toujours aussi difficile à dire.

Il y a plusieurs façons de se faire une idée :

- Sur ğchange : activez les annonces closes en cliquant sur “recherche avancée” et tapez dans la barre de recherche les mots clefs “euros” ou “UNL“. Certaines annonces que vous trouverez sur cette page de résultat correspondront à des ventes d’euros contre des Ğ1 (ou des achats d'euros contre des Ğ1).
- Sur le forum : vous trouverez des sujets comme celui-ci : “Enchère : 50 UNL” dans lequel l’auteur précise combien de Ğ1 il a pu obtenir en vendant des euros aux enchères
- En comparant le prix de biens vendus sur ğchange au même type de biens vendus sur des plate-formes de vente en euros, comme Le Bon Coin, par exemple.

# À titre d’exemple

Un FairPhone 2 neuf a été vendu 13 000 Ğ1 aux enchères le 16 septembre 2018, et un autre 12 048 Ğ1 le 23 décembre 2018. Son prix en euros tourne autour de 399,00 €.

En ce qui concerne les échanges de devises, voici certaines données historiques :

| Date             | Contexte                              | Vendeur d’€                                   | Vendeur de Ğ1        | Taux (Ğ1/€) |
| ---------------- | ------------------------------------- | --------------------------------------------- | -------------------- | ----------- |
| 9 octobre 2017   | annonce ğchange                       | cgeek                                         | N.C                  | 150         |
| 23 mars 2018     | annonce ğchange                       | Thierry                                       | Matiou,Lesipermanpot | 10 à 30     |
| 28 décembre 2018 | annonce ğchange                       | personne                                      | Gabriel              | 25          |
| 1er mars 2019    | enchère sur le forum g1-monetalibera.it | Looarn                                        | LucAstarius          | 30          |
| 10 mars 2019     | fin de Festi’June 2 Mayenne           | de nombreuses et nombreux ğunistes            | Le Sou               | 50          |
| continu          | magasin, ğmarchés                     | Djoliba (magasin de musique près de Toulouse) | les clients          | 5 puis 10   |
|Tous les mois en 2021|50€ chaquemois sur Gchange|Cukoland||≈20|