---
title: Moneta completa
description: "Quando il numero co-creatori di moneta sará stabile."
---
Nel mondo della moneta libera, ciò che chiamiamo moneta completa è il momento quando il numero medio di DU per membro sarà stabile. 
    
 Quando il numero di co-creatori non si alza quasi piú (N stabile), la rivalutazione del DUğ1 è molto vicino al 4,88% per semestre. 
 Quindi ogni sei mesi il numero di DUğ1 esistente è diviso per 1.0488. 

 Dopo circa 40 anni di esistenza, un conto di co-creatore arriva a circa 3.925 Duğ1 alla fine del semestre. L'applicazione della ri-valutazione (divisione per 1.0488) porta il conto intorno a 3742 DUğ1. 
 In un semestre un co -creator crea 182 o 183 DUğ1. 
 3742 + 183 = 3925 
 Durante tutto il semestre, la quantità di DU creato oscilla tra 3742 e 3925 per co-creatore di moneta. È impossibile creare più DUğ1. 

 Quindi la moneta completa è quando il numero di membri è stabile dopo almeno 40 anni.
