---
title: Théorie Relative de la Monnaie
description: Théorie monétaire utilisée comme fondement de la monnaie libre
synonyms:
  - trm
---
## Une Théorie et un livre

La théorie est décrite par Stéphane Laborde dans son livre *"la théorie relative de la monnaie"*.

### La théorie

Basée sur 4 axiomes : les <lexique>4 libertés économiques</lexique>.\
Pour respecter ces 4 libertés, la théorie décrit avec un calcul mathématique la création monétaire sous forme d'un dividende universel tel que : **DU=c(M/N)**.\
Le Dividende Universel 'DU' est une portion 'c' de la masse monétaire par individu 'M/N'.

### Le livre

Le livre est disponible en vente en ligne, mais également librement sur le site <http://trm.creationmonetaire.info/> Ou en PDF sur <https://www.econologie.com/fichiers/partager3/1319653067F3tO5k.pdf>
