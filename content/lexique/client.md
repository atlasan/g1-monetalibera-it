---
title: Client
description: Software per gli utenti della Ğ1
---
## Per gli utenti Ğ1
Per poter utilizzare il software Ğ1 hai bisogno di un software sul tuo computer o dispositivo mobile.

Clienti software Ğ1: 
 * [Cesium](https://cessium.app/fr/) Il più usato, esiste per quasi tutti i sistemi.
 * Sakia, un po' più complessa, che sarà presto sostituita da Tikka. 
 * Silkaj per linea di comando
 * Gecko non ancora disponibile, piuttosto focalizzato sui pagamenti. 
 * Tikka, sostituzione futura di Sakia 

 ## I clienti si connettono ai nodi. 
 I clienti selezionano più o meno automaticamente il <lexique>nodo</lexique> a cui si collegano. Per il momento césium offre all'utente di selezionare un nodo nella configurazione.
