---
title: DU
description: 'Dividendo Universale e unitá di misura.'
synonyms:
  - DU
  - dividendo universale
  - dividende universel
  - dividende
---

## Dividendo universale

Ogni co-creatore di moneta libera crea una quota di denaro proporzionale alla massa monetaria divisa per il numero di membri. 
 **DU = c×(M/N)**
 Questa creazione monetaria è chiamata Dividendo Universale o DU. 
 Questo è ciò che definisce una moneta come una moneta libera ai sensi della <lexique>TRM<lexique>.

 ## Unità di misura. 

 Nella moneta libera Ğ1 questo dividendo universale viene creato ogni giorno su ciascun conto di co-creatori di moneta (un solo conto da co-creatore per umano vivente). 
 La formula è quindi derivata come segue: 
 **DU<sub>(t+1)</sub>=DU<sub>(t)</sub> + c<sup>2</sup>×(M/N)<sub>(t)</sub>**. Questo DUğ1 viene rivalutato ogni 6 mesi (equinozi). 
 Questo DUğ1 è un invariante spazio-temporale: qualunque sia il luogo e il tempo un essere umano crea 1 DUğ1 al giorno, e questo DUğ1 rappresenta sempre la stessa parte della massa di denaro globale (approssimativamente). 

 Utilizzando questo DUğ1 giornaliero come unità di misurazione, l'evoluzione dei prezzi non dipende dall'offerta di moneta, ma da fattori al di fuori della valuta: offerta/domanda, tempo e altri imponderabili.
