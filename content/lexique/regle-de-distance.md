---
title: Règle de distance
description: Nous sommes tous reliés.
---

## Le monde est petit.

**Ce n'est pas une question de géographie, mais de lien**

Alice connaît Béatrice, Béatrice est à 1 pas d'Alice.
Béatrice connaît Chloé, Chloé est à 1 pas de Béatrice.
Chloé connaît Diane, Diane est à 1 pas de Chloé.
Alice ne connaît pas Chloé ni Diane, mais grâce à Béatrice, Chloé est à 2 pas d'Alice, et Diane est à 3 pas d'Alice

Une théorie prétend que nous sommes tous à moins de [six degrés de séparation](https://fr.wikipedia.org/wiki/Six_degr%C3%A9s_de_s%C3%A9paration) de n'importe qui dans le monde.

## Toile de confiance.

Nous sommes donc normalement tous peu éloignés les uns des autres grâce aux liens que nous avons de proches en proches. La toile de confiance ne fait que recenser dans la <lexique>blockchain</lexique> des liens tissés entre les êtres humains.   
Un compte cocréateur trop éloigné des autres serait celui de quelqu'un qui ne tisse pas assez de liens ou plus probablement un être imaginaire (un faux compte).   
D'où cette règle des 5 pas.   

Elle ne s'applique qu'à partir des membres <lexique title="référent">référents</lexique> (les membres ayant tissé de nombreux liens), et accepte que 20% de ces membres soient à plus de 5 pas. 

**Attention les pas ne sont comptés que dans un seul sens : **
Aline certifie Barbara. Barbara se trouve à un pas d'Aline.
Mais si Aline n'est pas certifiée par Barbara elle n'est pas à un pas de Barbara. Il faut que Barbara certifie Aline pour que cela fonctionne dans l'autre sens.
Alors inutile de certifier des référents pour s'en rapprocher, il faut s'en rapprocher pour qu'eux vous certifient.

## Résumé

Pour devenir et rester cocréateur de monnaie, il faut être à moins de 5 pas de 80% des cocréateurs <lexique title="référent">référents</lexique>.

Le pourcentage de membre référents à moins de 5 pas est appelé [*qualité* ou *distance*](/faq/quest-ce-que-la-qualite-de-membre-et-de-dossier).  
