---
title: Licenza
description: Un impegno da parte dei co-creatori
---
La licenza è un riepilogo delle regole da rispettare per diventare un membro della rete di fiducia. 

 Chiunque desideri partecipare alla co-creazione monetaria deve leggere, comprendere e accettare la [licenza](/licence-g1).

 È un impegno per tutti gli utilizzatori.