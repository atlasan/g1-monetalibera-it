---
title: Referenti
description: I co-creatori piú attivi nella rete di fiducia
---

I co-creatori referenti sono gli umani piú attivi nella <lexique title="RdF">rete di fiducia</lexique>, ovvero sono quelli che hanno ricevuto **ed** emesso un certo numero di certificazioni.

Un membro della <lexique>RdF</lexique> Ğ1 é membro referente se ha ricevuto ed emesso almeno Y[N] certificazioni dove N é il numero di membri della rete RdF e Y[N] = ceiling (N<sup>(1/5)</sup>).

Detto in altre parole: l'arrotondamento all'intero superiore della radice 5 del numero di membri.

Esempi:

- Per 1 024 < N ≤ 3 125 on a Y[N] = 5
- Per 7 776 < N ≤ 16 807 on a Y[N] = 7
- Per 59 049 < N ≤ 100 000 on a Y[N] = 10

Perché

- 5<sup>5</sup> = 3 125
- 6<sup>5</sup> = 7 776
- 7<sup>5</sup> = 16 807
- 9<sup>5</sup> = 59 049
- etc

*Attualmente un membro é referente se ha ricevuto almeno 6 certificazioni **e** emessi almeno 6 certificazioni.*

La <lexique>regola della distanza</lexique> é calcolata unicamente a partire da questi membri referenti.