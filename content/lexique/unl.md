---
title: UNL
description: Unités Non Libre (monnaies non libre)
---

## UNL ou MNL

Unité Non libre ou Monnaie Non Libre désigne toute monnaie qui ne respecte pas les <lexique>4 libertés économiques</lexique> axiomes définis dans la <lexique>Théorie Relative de La monnaie</lexique>.

Cela désigne le plus souvent toutes les monnaies fiats (monnaies émises par les états) et toutes les monnaies qui ne sont pas créées à parts égales entre tous les utilisateurs.

Le nom des autres monnaies est rarement utilisé, car on ne désire pas faire de la publicité pour des monnaies concurrentes.

Exemple :
Le terme UNLE : Unité Non Libre Européenne est utilisé pour désigner la monnaie de la zone Euro.
