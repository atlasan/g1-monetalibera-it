---
title: Forgiatori
description: Membre qui fait tourner Duniter
synonyms:
  - Forgeur
  - Forgiatori
---
La Ğ1 funziona grazie a una blockchain decentralizzata. 
Una blockchain decentralizzata funziona con i computer resi disponibili per la comunità. 
I forgiatori sono utenti che forniscono parte del proprio computer installando il software DUNITER.
Questi computer (istanze Duniter) sono chiamati<lexique>nodi</lexique>. 