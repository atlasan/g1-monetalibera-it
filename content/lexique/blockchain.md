---
title: Blockchain
description: Modalità di archiviazione e trasmissione di dati sotto forma di blocchi collegati tra loro e protetti da qualsiasi modifica.
---
Una blockchain è una tecnologia di registro distribuito che utilizza la crittografia per archiviare e proteggere i dati. È un sistema decentralizzato che consente il trasferimento sicuro di risorse digitali tra due o più parti. Ogni blocco contiene un hash crittografico del blocco precedente, insieme ai dati della transazione e a un timestamp. Questo crea una registrazione immutabile di tutte le transazioni che hanno avuto luogo sulla rete, assicurando che non possano essere alterate o cancellate.