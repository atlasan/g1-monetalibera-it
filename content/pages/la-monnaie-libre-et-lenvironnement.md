---
title: La monnaie libre et l'environnement
description: La  Ğ1 supporte très bien la décroissance et est sobre en énergie.
---
## La croissance.

La monnaie des banquiers est créée par les crédits accordés aux particuliers et aux états. Les crédits sont accordés selon la capacité de remboursement, c’est-à-dire l'aptitude à faire des profits. Car il faut en plus payer les intérêts.\
Cette monnaie des banquiers a besoin de croissance perpétuelle pour continuer d'exister. Sans croissance, il n'y a plus de crédits, et sans crédit la monnaie disparaît. 

La monnaie libre est créée en continu tant qu'il y a des êtres humains vivants. Elle n'a donc pas besoin de croissance, ni de course au profit pour continuer d'exister.\
Ainsi elle peut être la monnaie idéale pour accompagner une décroissance vers un monde plus sobre. 

## La sobriété

La Ğ1 n'utilise pas de billets infalsifiables coûteux en ressources, elle préfère utiliser la blockchain.\
L'algorithme de la Ğ1 a été particulièrement étudié pour consommer très peu d'énergie.\
<https://duniter.fr/faq/duniter/duniter-est-il-energivore/>\
La prochaine version sera encore plus économe.