---
title: Comprendere
description: Spiegazioni teoriche
plan: ui/main-flatplan
---
## La TRM

Il moneta libera è la concretizzazione della teoria relativa della moneta scritta nel 2010 da Stéphane Laborde.

### 4 libertá

Questa teoria pone 4 assiomi che sono le 4 libertà economiche

1. Libertà di scelta del sistema monetario 
2. Libertà di utilizzare le risorse 
3. Libertà di stima e produzione di qualsiasi valore economico 
4. Libertà di scambiare, contare, mostrare i suoi prezzi "in valuta"

La libertà s'intende come non nociva per se stessi e per gli altri. Se c'è un danno, c'è una privazione della libertà.

### Rinnovo delle generazioni

![Gli umani del passato non esistono più, gli umani del futuro non esistono ancora](https://www.creationmonetaire.info/wp-content/uploads/2013/05/TRM35_6.png "Spazio-Tempo umano (durata da 0 ad aspettativa di vita)")
I vivi che arrivano alla fine della lore vita (ev≈80anni), sono stati in grado di scambiare con gli umani del passato (in rosso), ma non scambieranno mai con gli umani del futuro.\
Gli umani all'inizio della vita non hanno mai scambiato con gli umani del passato, ma un giorno scambieranno con gli umani del futuro (in giallo).\
Solo gli umani viventi (in verde) possono scambiare tra di loro, non c'è motivo per cui i loro scambi influenzino le generazioni future,
né che siano influenzati dagli scambi delle generazioni passate.

### Uguaglianza spazio-temporale

Per rispettare le 4 libertà, ogni essere umano deve creare la stessa quota di moneta (la stessa porzione).\ 
 Ogni generazione crea la moneta che utilizza senza che questa moneta abbia un impatto sulle generazioni future.

### La formula

DU=c(M/N)\
La parte che ogni essere umano crea (DU, dividendo universale) è una parte, un coefficiente (c) della media della massa monetaria per membro (M/N).\ 
Questo coefficiente **c** deve essere vicino al 10% annuo per non favorire il più giovane o più vecchio.

### Convergenza dei conti

Ogni creatore crea la stessa quantità di denaro, i loro conti si avvicinano nel tempo. Come la differenza d'età diventa sempre meno visibile con man mano che si invecchia.

![](/uploads/convergence-des-soldes.png)


## Maggiori informazioni

#### La TRM

Per saperne di più si puó leggere la teoria relativa della moneta. Questo libro scritto da Stéphane Laborde è disponibile online : [http://trm.creationmonetaire.info/](http://trm.creationmonetaire.info/)   


#### La TRM nel dettaglio

Emmanuel Bultot, dottore in Matematica, ha pubblicato [« la TRM en détail »](http://monnaie.ploc.be/#trm-en-detail), una rivista della Teoria Relativa della Moneta da un nuovo punto di vista, molto ben fatto e altamente raccomandato.


#### La TRM per i bimbi

David Chazalviel, ingegnere informatico, ha realizzato [« la TRM pour les enfants »](http://cuckooland.free.fr/LaTrmPourLesEnfants.html) Una spiegazione della TRM interattiva più facile da capire.


### Video

Un video esplicativa della TRM (durata 1 h 15, lingua originale).

<iframe width="560" height="315" src="https://www.youtube.com/embed/PdSEpQ8ZtY4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## La Ğ1

La Ğ1 (la "Giuna", in Francese "June", in Spagnolo "Juna") é la prima MONETA LIBERA della storia dell'umanitá.\
In conformità con la Teoria Relativa della Moneta (TRM), è co-creata senza debiti ed in parti uguali, tra tutti gli esseri umani di tutte le generazioni attuali e future, sotto forma di un "pacchetto" di moneta, una quantità di Ğ1 (una quantità di "giune"), chiamato DIVIDENDO UNIVERSALE (DU).\ 
I bambini sono esseri umani completi, partecipano alla creazione monetaria creando la loro parte di moneta ogni giorno!


### I DUğ1

Per la moneta libera G1 il DU viene calcolato tutti i semestri (182.625 giorni), il coefficiente è quindi del 4,88% per semestre ed è distribuito ogni giorno. 
Ogni giorno, ogni co-creatore crea una piccola porzione di Dividendo Universale.\ 
Essendo la massa monetaria di partenza zero, il primo DU è stata fissata arbitrariamente a 10 Ğ1 al giorno, e poi aumenta.


### La formula adattata

La formula del calcolo del DUğ1 é : **DU<sub>t+1</sub> = DU<sub>t</sub> + (c<sup>2</sup> × (M/N)<sub>t</sub> / 182.625)**\
Questo calcolo viene eseguito ogni sei mesi (182,625 giorni) e tiene conto dell'evoluzione del numero di membri durante la fase di installazione della moneta.


### La blockchain
La tecnologia blockchain è stata scelta per la sua semplicità di implementazione, sicurezza e decentralizzazione.\ 
Contrariamente alla credenza popolare, non è la blockchain che consuma energia, ma la "prova del lavoro".\ 
Per la Ğ1 l'algoritmo è stato [**progettato per consumare pochissima energia**] (https://duniter.fr/faq/duniter/duniter-est-il-energivore/), è sufficiente un computer delle dimensioni di un pacchetto di sigarette per calcolare i blocchi, molto lontano dall'immensa quantitá necessaria ad esempio per bitcoin.

### La rete di fiducia
Essendo la moneta creata sui conti degli utenti membri, è necessario garantire che tutti abbiano un solo account creatore di moneta. 
Per rispettare la decentralizzazione e non dare potere a nessun organismo, sono gli stessi membri che identificano gli altri membri
