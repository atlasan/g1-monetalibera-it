---
title: Contribuire
description: Tutte le informazioni per partecipare!
plan: ui/main-flatplan
---
## Cambiano il mondo

A volte sentiamo qualche <lexique>giunista</lexique> dire que la Ğ1 é "*un'opera d'arte collettiva*".

Ed è vero che ci sono molte persone che fanno cose intorno alle monete gratuite in generale o in particolare.

È un'intera sinfonia che si svolge, senza che nessuno lo sappia troppo.

Se il ğ1 esiste oggi, è grazie a tutte queste persone: 

Coloro che sviluppano software: troverai i loro nomi sul sito [Duniter](https://duniter.org/fr/), il motore della blockchain. 

Coloro che convalidano le transazioni della blockchain mediante [blocchi di forgiatura](https://duniter.org/fr/miner-des-blocs/). 

Coloro che traducono il software. 

Coloro che scrivono documentazione. 

Coloro che scrivono segnalazioni di bug.

Coloro che sviluppano l'economia: questi sono tutti coloro che, [con i loro scambi](https://www.gchange.fr/#/app/market/lg?last), valutano la Ğ1 e dimostrano quotidianamente che una valuta libera ti permette di scambiare. 

 Coloro che ne parlano intorno a loro. 

 Coloro che creano gruppi locali. 

 Coloro che organizzano parti di [ğeconomicus](http://geecomicus.glibre.org/). 

 Coloro che creano meme divertenti. 

 Coloro che danno conferenze per spiegare la creazione monetaria. 

 Coloro che creano contenuti educativi online. 

 Coloro che scrivono libri. 

 Coloro che producono la creazione grafica. 

 Coloro che forniscono infrastrutture per accogliere i contenuti. 

 E tutti quelli che questo elenco ha dimenticato di elencare.


## Sta a te ! 

La Ğ1 non appartiene a nessuno. 

Inoltre, se ti è stato detto che questo sito era il "*sito ufficiale*" della moneta libera o Ğ1, allora lasciaci chiarirlo subito: né la moneta libera né la Ğ1 possono avere “*sito ufficiale*“, perché non esiste autorità che li governi. 

Certo, questo sito è spesso quello che esce per primo nei motori di ricerca quando digiti "*moneta libera*", ma altri siti parlano di monete libere o Ğ1 e non sono meno legittimi di questo per farlo. 

Alcune persone potrebbero provare a depositare il marchio "moneta libera", ma probabilmente si dovranno ricredere, perché "moneta libera" è solo l'associazione di due nomi comuni.

La Ğ1 non ha testa. 

La Ğ1 non ha centro. 

Sei quindi libero di contribuire come ritieni opportuno, senza aspettare che nessuno ti dia l'autorizzazione. 

Se vuoi collaborare con altre persone su determinati progetti, puoi presentarti su qualsiasi piattaforma o rete su quali ci siano giunisti: i giunisti generalmente sapranno come metterti in contatto con altri giunisti in modo da poter unire gli sforzi 😉


## Fare donazioni. 

### Aona a DUğ1. 
Puoi dare DUğ1 ai collaboratori tecnici e <lexique title="forgiatori">Forgiatori</lexique> copiando le seguenti chiavi: 
Contributori tecnologici. DUNITER/Ğ1: 78ZwwgpgdH5uLZLbThUQH7LKwPgjMunYfLiCfUCySkM8 
Remuniter, per i forgiatori: TENGx7WtzFsTXwnbrPEvb6odX2WnqYcnnrjiiLvp1mS 

Esistono un sacco di premi di contributo per vari progetti, troverai un [elenco non esaustivo sul forum](https://forum.g1-monetalibera.it/t/liste-des-cagnottes-de-cotisations-de-la-g1/13828).
Fai attenzione, prima di fare una donazione, assicurati chi gestisca davvero il progetto. 

### Dando altre monete 
Ci sono anche associazioni che aiutano lo sviluppo di Ğ1 e accettano donazioni e contributi in altre valute: 
[axiom-team](https://axiom-team.fr/adherer)    
[éconolibre](https://www.helloasso.com/associations/econolibre/formulaires/1/widget)  
Qualsiasi aiuto anche minimo è il benvenuto. 


## Supporta finanziariamente il Ğ1v2 
La Ğ1 si evolve, aiuta finanziariamente gli sviluppatori della Ğ1 del futuro. 
[CrowdFunding](https://duniter.fr/blog/financement-participatif/)   
Via [Hello asso](https://www.helloasso.com/associations/axiom-team/collectes/financement-de-la-g1v2) 31/12/2022
