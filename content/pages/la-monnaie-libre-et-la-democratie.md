---
title: La monnaie libre et la démocratie
description: Qu'est-ce que la monnaie libre peut apporter à la démocratie ?
---
## Démos (le peuple) Kratos (le pouvoir)

Nous savons que la principale source de pouvoir vient de la monnaie. Plus vous possédez de monnaie plus vous avez de pouvoir.\
Nous savons que le temps de présence dans les médias influe directement sur les résultats aux élections. Les médias sont détenus par les plus grosses fortunes, ce sont donc les plus gros possesseurs de monnaie qui décident du résultat des élections.
![](http://etienne.chouard.free.fr/Europe/Correlation_heures_TV_resultats_election.jpg)

## Reprendre le pouvoir.

Avec la monnaie libre, le choix de création et l'injection de monnaie dans l'économie pour financer une chose ou une autre, n'est plus le privilège des banquiers ou des ministres (via la création monétaire qui se fait actuellement par crédit bancaire), mais elle est réalisée par chaque individu, via le DU qui est créé chaque jour (sans dette ni contre-partie de quelque nature que ce soit) sur son compte cocréateur, et qu'il injecte lui-même dans l'économie quand il dépense ses Ğ1 nouvellement créées. Ce faisant, il vote pour et exprime ses valeurs, et influence lui-même et directement la société dans laquelle il s’inscrit.

## Égalité.

Avec la création monétaire à égalité pour tous et la convergence des comptes vers la moyenne, le pouvoir n'est plus entre les mains de quelques uns, mais entre les mains de tous.\
Chacun peut avoir le même poids dans le système.