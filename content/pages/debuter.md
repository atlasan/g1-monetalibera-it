---
title: Débuter
description: Par où commencer pour utiliser la Ğ1
plan: ui/main-flatplan
---
## Ouvrir un compte

<alert type="info">Commencez toujours par un *compte simple portefeuille*, ce n'est **qu'après avoir rencontré** plusieurs fois d'autres cocréateurs que vous pourrez demander à [devenir *membre* cocréateur de monnaie](/faq/comment-devenir-cocreateur)</alert>

### 1. Téléchargez césium

Sur votre ordinateur, rendez-vous sur le site <https://cesium.app/fr/>
Allez sur la page "téléchargement" pour choisir la version qui vous convient.
Nous vous conseillons de choisir l'extension correspondant à votre navigateur internet.
Si vous choisissez une extension, vous verrez apparaître le logo césium au bout de votre barre d'adresse.
Si vous choisissez une application, le logo césium apparaîtra sur votre bureau.

Sur votre smartphone, allez dans votre "store", cherchez l'application *cesium G1*, et installez-la.

### 2. Modifier les paramètres par défaut.

Il y a des <lexique>nœuds</lexique> configurés par défauts, si tout le monde reste sur ces nœuds par défauts, ceux-ci seront surchargés et ça rame.
Voir la procédure indiquée sur le [forum](https://forum.g1-monetalibera.it/t/noeud-cesium-vs-noeud-duniter/14762).
Vérifiez que vous êtes sur un nœud à jour : [synchronisation des nœuds](https://forum.g1-monetalibera.it/t/synchronisation-des-noeuds/14780).

Les prochaines versions d'applications de gestion de portefeuilles feront cela de façon automatique. En attendant faut faire ça à la main.

*Choisissez l'affichage en DUğ1.*
Il y a de nombreuses discussions sur la pertinence de compter en [ğ1 ou en DUğ1](/faq/lunite-cest-le-dug1-ou-la-june-g1).
L'affichage en DUğ1 vous ouvre à une nouvelle manière de penser.

*Jouez avec les différents paramètres pour voir ce que cela change à l'affichage*

Visitez les différents onglets pour vous rendre compte de tout ce qui est visible par une personne non inscrite.

### 3. Choisir un identifiant / mot de passe

La connexion à votre compte se fait avec deux codes secrets : un identifiant **secret** et un mot de passe.
Ces deux parties du code secret doivent rester secrètes même l'identifiant.
Pour une plus grande sécurité, il est préférable d'avoir des codes secrets assez longs de l'ordre de 25 caractères ou plus. Nous parlerons de phrases de passe.
Pour plus de détail, vous pouvez consulter le tutoriel du site césium.app ["Comment créer un compte sécurisé ?"](https://cesium.app/fr/tutoriel-cesium/creer-un-compte/compte-membre/comment-creer-compte-securise/)

<alert type="warning">Il n'existe aucun système de récupération de "mot de passe oublié". Vous devez donc soigneusement noter vos phrases de passe </alert>

<alert type="info">Vous pouvez aussi choisir d'utiliser un gestionnaire de mots de passe</alert>

### 4. Ouvrir le compte

Une fois choisies et mémorisées vos phrases de passe, vous pouvez ouvrir votre compte.
Le passage par le bouton "créer un compte" n'est pas indispensable. Si vous avez bien mémorisé vos phrases de passe, vous pouvez directement vous connecter. Si vous passez par ce bouton, choisissez "simple portefeuille".
Après avoir tapé vos phrases de passe, vous verrez apparaître votre clé publique. Il est conseillé de *recommencer plusieurs fois l'opération déconnexion-reconnexion* pour s'assurer que c'est toujours la même clé publique qui s'affiche.
Lorsqu'une clé publique différente apparaît, c'est que vous avez fait une faute de frappe.

<alert type="warning"> Le logiciel est sensible à la casse, une erreur de majuscule ou d'accent ou ajout d'espace ou n'importe quel caractère change la clé. Soyez vraiment précis.</alert>

<alert type="info">Votre compte sera d'abord un **compte simple portefeuille**. C'est-à-dire qu'il ne sera pas créateur de monnaie. Il faut participer aux échanges **avant** de [devenir cocréateur de monnaie](/faq/comment-devenir-cocreateur)</alert>

### 5. Renseigner votre profil (ou pas)

Vous pouvez garder votre compte complètement anonyme.
Vous pouvez aussi renseigner un nom, un prénom ou un pseudo pour aider vos partenaires à vous retrouver lorsqu'ils voudront faire un paiement.
Vous pouvez aussi indiquer votre adresse plus ou moins partiellement en vous géolocalisant, pour que les personnes de votre région puissent vous retrouver sur la carte.

Cela se fait sur la page mon compte en cliquant le bouton "Saisir mon profil". Vous pourrez toujours modifier ou supprimer ce profil.

<alert type="warning">Les informations saisies dans votre profil sont visibles par tout le monde, y compris des personnes n'appartenant pas à la monnaie libre. Évitez de laisser trainer des informations trop personnelles sur internet.</alert>

### 6. Activer les notifications par mail

Pensez à activer le service de notification par mail, pour recevoir un message si quelqu'un essaie de vous contacter via césium, par exemple un voisin qui vous aurait trouvé sur la carte.
Ce service de notification se fait dans la partie "service en ligne" sur la page "mon compte" en cliquant sur "Ajouter un service".
Attention, il n'y a pas de vérification de votre adresse mail, ne faites pas d'erreur de saisie.

## Participer aux rencontres

Pas besoin d'être certifié pour utiliser la monnaie et faire des échanges.
**Le plus important maintenant que vous avez créé votre compte, c'est de l'utiliser !**
Vous devez participer aux rencontres pour faire des échanges et faire vivre cette nouvelle monnaie.
Au départ votre compte est vide, pour "gagner" vos premières Ğ1 vous devez commencer par vendre. Videz vos placards des choses devenues inutiles pour vous, ou proposez vos propres créations artisanales ou culinaires. Vous pouvez aussi proposer des services.

* Sur le forum, vous trouverez [le calendrier des rencontres](https://forum.g1-monetalibera.it/calendar)
* Sur le forum, un sujet épinglé en haut de chaque catégorie, récapitule les divers canaux de communication.
* Un petit tuto pour [ne pas rater une rencontre](https://forum.g1-monetalibera.it/t/comment-ne-pas-rater-une-rencontre/7408/1) sur le forum. Adaptez-le à votre région.
* Sur la [page d’accueil](/#agenda), vous avez tous les évènements indiqués sur le forum. Cherchez ceux près de chez vous. Vous pouvez proposer vos propres évènements, inviter des <lexique>junistes</lexique> à venir vous voir.
* Chercher les groupes dans votre région : [Voir la carte des évènements et groupes locaux](https://carte.g1-monetalibera.it?members=false)
* Utilisez [ğchange](https://www.gchange.fr/#/app/home), pour poster vos petites annonces de biens ou services. N'hésitez pas à faire des demandes.
* Accueillez des junistes en vacances avec [airbnjune](https://airbnjune.org/)


## Vidéo :

### Création d'un compte monnaie libre sur Césium

Vidéo tutoriel sur comment créer un compte monnaie libre sur l'application Césium.\
Cela suppose que vous avez déjà téléchargé et installé Césium sur votre ordinateur, tablette ou Smartphone via [https://cesium.app](https://cesium.app/)

<iframe width="100%" height="405" sandbox="allow-same-origin allow-scripts allow-popups" title="Création d&#39;un compte monnaie libre sur Césium" src="https://tv.adn.life/videos/embed/6bfafdbf-9caf-4645-984d-64a11b5fd49a" frameborder="0" allowfullscreen></iframe>

### Plus de tuto en vidéo
Bruno de la Réunion a fait quelques petits [tutos vidéos](https://video.tedomum.net/c/monnaie_libre_974_g1/videos)

## Allez-y mollo !

La Ğ1 est un projet récent (elle est née en mars 2017).

Considérez la Ğ1 comme une expérimentation grandeur nature.

Ne mettez pas tous vos espoirs dans la Ğ1.

Il est possible que vous trouviez quelques avantages à utiliser la Ğ1, mais en aucun cas la Ğ1 ne résoudra tous vos problèmes.

## Vous avez des questions ?

* Consultez la [foire aux questions](/faq) de ce site.
* Cherchez d'autres réponses sur le [forum](https://forum.g1-monetalibera.it/)
* Posez vos questions en direct lors des [visioconférences](https://forum.g1-monetalibera.it/t/organisation-dune-chaine-visios/13044)
