export default {
  'content:file:beforeParse': (file) => {
    // Netlifycms cannot create array in json file at root level and nuxt-content need an array. So, ressources.json is parsed to fetch 'ressources' key.
    if (file.extension === '.json' && /.*ressources.json$/.test(file.path)) {
      file.data = JSON.stringify(JSON.parse(file.data).ressources)
    }

    // Split md file with `---`. Used for flat plan.
    if (file.extension === '.md' && /^---.*layout:.*---/s.test(file.data)) {
      let t = 0
      const data = file.data.replace(/---/g, (match) => {
        ++t
        if (t === 2) return '---\n<section>'
        if (t > 2) return '</section>\n<section>'
        return match
      })
      file.data = data + '\n</section>'
    }
  },
  async 'content:file:beforeInsert'(document) {
    if (document.extension === '.md') {
      /*
       * Add reading time
       */
      document.readingTime = require('reading-time')(document.text)

      /*
       * Add lexique terms and check if terms exist
       */
      // Search lexique terms
      const lexiqueTerms = [
        ...document.text.matchAll(
          /<lexique[>]([^<]*)<\/|<lexique\s+title="(.*)"/g
        ),
      ].map((match) => {
        return (match[1] || match[2]).toLowerCase() // Priority for term in title=""
      })

      if (lexiqueTerms.length) {
        // Add terms to document
        document.lexiqueTerms = [...new Set(lexiqueTerms)] // remove duplicate

        const { $content } = require('@nuxt/content')
        // Check if term exists
        if ($content) {
          for (let i = 0; i < document.lexiqueTerms.length; i++) {
            const term = document.lexiqueTerms[i]
            const lexique = await $content('lexique')
              .where({
                $or: [
                  { title: { $regex: [`^${term}$`, 'i'] } }, // case insensitive
                  { synonyms: { $contains: term } }, // search in synonyms
                ],
              })
              .limit(1)
              .fetch()

            if (!lexique.length) {
              // eslint-disable-next-line no-console
              console.warn(
                `Term "${term}" in file ${
                  document.path + document.extension
                } doesn't exist`
              )
            }
          }
        }
      }
    }
  },
}
