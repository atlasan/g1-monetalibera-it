/*
https://cdn.rawgit.com/Marak/faker.js/master/examples/browser/index.html#lorem

Generate files:
    node test/generate-md-files.js
Remove files:
    cd content/faq
    rm fake-*.md
*/

const fs = require('fs')
const path = require('path')
const faker = require('faker/locale/fr')

const numberOfFiles = 50

for (let i = 0; i < numberOfFiles; i++) {
  generateFAQ()
}

function generateFAQ() {
  const outputDir = 'content/lexique'

  const fileName = faker.lorem.word() + '-' + faker.lorem.word()
  const fileContent = `---
title: ${faker.lorem.sentence()}
description: ${faker.lorem.sentences()}
---
${faker.lorem.paragraphs()}
`
  writeFile(`${outputDir}/fake-${fileName}.md`, fileContent)
}

function writeFile(outputPath, fileContents) {
  fs.writeFile(
    path.join(__dirname, `../${outputPath}`),
    fileContents,
    function (err) {
      if (err) return console.warn(err)
      console.log(outputPath + ' file generated')
    }
  )
}
